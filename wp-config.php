<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tskold_production');

/** MySQL database username */
define('DB_USER', 'tskold_admin');

/** MySQL database password */
define('DB_PASSWORD', 'lovenotwar');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gAGx4Nj:CFmKP?+p^e6047%/)K8@qT-H^R=0;-:!YacO/M,4-tti4u| g|GyLeV4');
define('SECURE_AUTH_KEY',  'PA4zaJ1:iyb5Z`95;BO4-tfRi{%}BLb1It`}=}C5rqW{PcihUiofP5;0Kd9H9QA?');
define('LOGGED_IN_KEY',    '/+MsfU{h$>ZhY6c?A`9)2>XhF4qtZ((q2$TbV~,|-# S$SZlm9GBhb<0>1?yg&O3');
define('NONCE_KEY',        'Oc=38^Q#p9YK)c&2%G:@}(^yIZnvrX-QDk=aEa/|7su}8GKjI+Z{o|99| 0}L`<K');
define('AUTH_SALT',        'oLB=onx3gc>=wIq+O*?F:)ge=v.Asuyl[B-5VP{^pF^M[lbXwI5|U$w6lZ9zv%f5');
define('SECURE_AUTH_SALT', 'Q<)1d~)lbzv?`DH|jbEnyifdnizt$JD|AA|_8YX|):M:XFwOr.5%Gjl-8X!:I_|O');
define('LOGGED_IN_SALT',   'T6N8ZBG9Q-}.)S(6QRuH)DN{?wyJw1ZMn&|k@S)>z0$.HdCLG)ovw!E6@-iL_n+n');
define('NONCE_SALT',       'IzJ<g|HrCr]k.|qirpi7ePm9<gN=g+B^iL3>er3CrgVE!|+-*I|I.Z*h)[j|7dtg');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
